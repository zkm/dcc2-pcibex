# DCC 2 and 3
This version of the study presents one guise, and instructions are given in speech bubbles. 
All that differs between DCC2 and DCC3 is the CSV of items (*was/were* critical items for DCC2, verbal *-s* critical items for DCC3.)

# Sync
Sync your Ibex project with this repository to get the latest PennController.js file

**WARNING:** the `master` branch will override any *example_data.js* file. Use the `no-example` branch to only update the *PennController.js* file.