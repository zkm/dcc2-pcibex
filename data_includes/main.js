// This is a simple demo script, feel free to edit or delete it
// Find a tutorial and the list of availalbe elements at:
// https://www.pcibex.net/documentation/

PennController.ResetPrefix(null) // Shorten command names (keep this line here)
PennController.DebugOff() // Comment out to use Debugger


// Define function to insert break
// Created by Jeremy Zehr on this help page: https://www.pcibex.net/forums/topic/catch-trials/#post-5643
function SepWithN(sep, main, n) {
    this.args = [sep,main];

    this.run = function(arrays) {
        assert(arrays.length == 2, "Wrong number of arguments (or bad argument) to SepWithN");
        assert(parseInt(n) > 0, "N must be a positive number");
        let sep = arrays[0];
        let main = arrays[1];

        if (main.length <= 1)
            return main
        else {
            let newArray = [];
            while (main.length){
                for (let i = 0; i < n && main.length>0; i++)
                    newArray.push(main.pop());
                for (let j = 0; j < sep.length; ++j)
                    newArray.push(sep[j]);
            }
            return newArray;
        }
    }
}
function sepWithN(sep, main, n) { return new SepWithN(sep, main, n); }



// Set counter at beginning so that simulatenous participants don't get the same group assignment
SetCounter("counter", "inc", 1);

// Show the 'intro' trial first, then all the 'experiment' trials in a random order
// then send the results and finally show the trial labeled 'bye'
Sequence("counter", "consent", "training_sb", "training", "attention", "exp_instr_sb", sepWithN( "break", "experiment_block1", 30),
    "guiseq", "demo_html", "region", "comment_page", SendResults() , "bye" )
//Sequence("training", "consent", "training_sb", "training", "attention", "exp_instr_sb",  SendResults(), "bye")
PennController.AddHost("http://terpconnect.umd.edu/~zach/dcc/")

// What is in Header happens at the beginning of every single trial
Header(
    // We will use this global Var element later to store the participant's name
    newVar("ParticipantName")
        .global()
    ,
    // Delay of 250ms before every trial
    newTimer(250)
        .start()
        .wait()
)
//.log( "Name" , getVar("ParticipantName") )
// This log command adds a column reporting the participant's name to every line saved to the results

//Consent
PennController("consent",
     newHtml("consent-form", "consent.html")
        .print()
     ,

     //log Prolific ID
     newText("Prolific ID:")
        .bold()
        .center()
        .print()
    ,
    newTextInput("prolificID")
        .settings.log()
        .settings.lines(1)
        .center()
        .print()
        .log()
    ,
    newText(" ")
        .print()
    ,
     newButton("consent-button", "I consent.")
        .center()
        .print()
        .wait()
);

// Training Instructions
PennController("training-instr",
     newHtml("instructions_training.html")
        .print()
     ,
     newButton("Start Practice")
        .center()
        .print()
        .wait()
);




// Declare variables for image and canvas size in SPR
let img_dim = 175;
let box_dim = 185;
let spr_canvas_dim = 400;
let img_left = 5;
let img_right = 220;
let img_top = 5;
let img_bottom = 220;
let canv_width = 598;
let canv_height = 308;

// Speech bubble image
let sb_width = 391;
let sb_height = 308;
let sb_x_left = 0;
let sb_x_right = 207;

// DashedSentence Placement
let ds_x_left = 240;
let ds_x_right = 240;
let ds_y = -10;

let guise_name_x_left = 80;
let guise_name_x_right = 390;
let guise_name_y = 200;


// Training (moving window) speech buble
let canv_wide_width = 750;
let canv_wide_height = 250;
let sb_wide_width = 750;
let sb_wide_height = 241;
let sb_wide_x_right = 10;
let sb_wide_x_left = 185;
let sb_wide_y = 20;
let sb_wide_text_left = 200;


//Training instructions with speech bubble
PennController("training_sb",
    newImage("sb_instr", "training_sb.png")
        .settings.size(sb_wide_width, sb_wide_height)
    ,
    newCanvas("instr_canvas", canv_wide_width, canv_wide_height)
        .center()
        .add(0, 0, getImage("sb_instr"))
        .print()
    ,
    newText("instr1", "Welcome to the experiment!<br>I'm Jordan, and I'm gonna tell you what to do.")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("instr_canvas"))
    ,
    newButton("instr_button1", "Next")
        .center()
        .print()
        .wait()
    ,
    getText("instr1").remove()
    ,
    getButton("instr_button1").remove()
    ,
    newText("instr2", "Someone will say a sentence one word at a time. <br>Pay attention to the sentence!") //<br>You've gotta press the space bar to see each word.")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("instr_canvas"))
    ,
    newButton("instr_button2", "Next")
        .center()
        .print()
        .wait()
    ,
    getText("instr2").remove()
    ,
    getButton("instr_button2").remove()
    ,
    newText("instr3", "After each one, you'll click on the picture that best matches the sentence.<br>Then, you'll answer a question about the sentence.")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("instr_canvas"))
    ,
    newButton("instr_button3", "Next")
        .center()
        .print()
        .wait()
    ,
    getText("instr3").remove()
    ,
    getButton("instr_button3").remove()
    ,    
    newText("instr4", "I'll say a few sentences first to help you practice.<br><br>After each sentence, you'll see the correct answer to help you understand the task.")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("instr_canvas"))
    ,
    // newButton("instr_button4", "Next")
    //     .center()
    //     .print()
    //     .wait()
    // ,
    // getText("instr4").remove()
    // ,
    // getButton("instr_button4").remove()
    // ,
    // newText("instr5", "Try to go as quickly as you can while fully understanding the sentences!")
    //     .print(sb_wide_text_left, sb_wide_y, getCanvas("instr_canvas"))
    // ,
    newButton("instr_button5", "Start Practice")
        .center()
        .print()
        .wait()
);

// Training
Template( "dcc2_training.csv" ,
    row => newTrial( "training",
        // newText("trainInstr1", "Press the space bar to read each word.\n\n")
        //     .italic()
        //     .center()
        //     .print()
        // ,    
        newImage("sb1", "training_sb_inplace.png")
            .settings.size(sb_width, sb_height)
        ,
        newController("ds1", "DashedSentence", {s: row.SentenceText, mode: "speeded acceptability", blankText: "+", display: "in place"})
        ,
        newCanvas("sprcanv", canv_width, canv_height)
            .center()
            .add( sb_x_left, 0, getImage("sb1"))
            .add(ds_x_left, ds_y , getController("ds1"))
            .print()
        ,
        getController("ds1")
            .wait()
        ,
        getCanvas("sprcanv").remove()
      //  getText("trainInstr1").remove()
    ,
   // Picture verification                 
    newImage('imageTL' ,  row.ImageTL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageTR' ,  row.ImageTR)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBL' ,  row.ImageBL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBR' ,  row.ImageBR)
        .settings.size(img_dim, img_dim)
    ,
    newText("trainInstr2", "Select the image that matches the sentence you just read.\n\n")
        .italic()
        .center()
        .print()
    ,    
    newCanvas( 'myCanvas', spr_canvas_dim, spr_canvas_dim)
        .center()
        .settings.add( img_left, img_top, getImage('imageTL'), 0 )
        .settings.add( img_right, img_top, getImage('imageTR'), 1 )
        .settings.add( img_right, img_bottom, getImage('imageBR'), 2 )
        .settings.add( img_left, img_bottom, getImage('imageBL'), 3 )
        .print()
     ,

    // Log canvas reaction time
    newVar("RT").global().set( v => Date.now() )
    ,
    newSelector( 'mySelector')
        .add( getImage('imageTL') , getImage('imageBL') , getImage('imageBR') , getImage('imageTR') )
        .wait()
        .log()
    ,
    getVar("RT").set( v => Date.now() - v )
    ,
    // Feedback
    newImage('greenRect', "green_rectangle.png")
        .settings.size(box_dim, box_dim)
        .print(row.CorrectX, row.CorrectY, getCanvas("myCanvas"))
    ,
    // newText("feedbackPrompt", "Sentence:")
    //     .bold()
    //     .center()
    //     .print()
    // ,
    // newText('feedbackText', row.SentenceText)
    //     .center()
    //     .print()
    // ,
    newTimer(500)
        .start()
        .wait()
    ,
    // Remove canvas
    getText("trainInstr2").remove()
    ,
    getCanvas("myCanvas").remove()
    ,
    // getText("feedbackPrompt").remove()
    // ,
    // getText("feedbackText").remove()
    // ,
    // Ask comprehension question
    newText(row.Question)
        .bold()
        .center()
        .print()
    ,
    newText("option1", row.Option1)
    ,
    newText("option2", row.Option2)
    ,
    newScale("comp_question", getText("option1"), getText("option2"))
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .center()
        .print()
    ,
    newButton("Check answer")
        .center()
        .print()
        .wait()
    ,
    newText("Correct Answer: ")
        .bold()
        .color("green")
        .center()
        .print()
    ,
    newText("comp_feedback", row.CorrectAnswer)
        .center()
        .color("green")
        .print()
    ,
    newTimer(750)
        .start()
        .wait()
    )

    .log("List", "Training")
    .log("Guise", "Training")
    .log("ItemType", "Training")
    .log("ItemID", "Training")
    .log( "Cond", "Training")
    .log( "SentenceText", row.SentenceText)
    .log( "ImageTL", row.ImageTL)
    .log( "ImageTR", row.ImageTR)
    .log( "ImageBL", row.ImageBL)
    .log( "ImageBR", row.ImageBR)
    .log( "LabelTL", row.LabelTL)
    .log( "LabelTR", row.LabelTR)
    .log( "LabelBL", row.LabelBL)
    .log( "LabelBR", row.LabelBR)
    .log( "ReactionTime", getVar("RT"))
    .log( "CompQ", row.Question)
    .log( "CompQ_Order", row.CorrectChoice)
    .log( "CompQ_Answer", row.CorrectAnswer)
    .log( "CompQ_Foil", "Dummy")
)

// Attention check
newTrial( "attention",
        // newText("trainInstr1", "Press the space bar to read each word.\n\n")
        //     .italic()
        //     .center()
        //     .print()
        // ,    
        newImage("sb1", "training_sb_inplace.png")
            .settings.size(sb_width, sb_height)
        ,
        newController("ds1", "DashedSentence", {s: "Please select three to show you are paying attention.", mode: "speeded acceptability", blankText: "+", display: "in place"})
        ,
        newCanvas("sprcanv", canv_width, canv_height)
            .center()
            .add( sb_x_left, 0, getImage("sb1"))
            .add(ds_x_left, ds_y , getController("ds1"))
            .print()
        ,
        getController("ds1")
            .wait()
        ,
        getCanvas("sprcanv").remove()
    ,

   // Picture verification                 
    newImage('imageTL' ,  "one.png")
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageTR' ,  "two.png")
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBL' ,  "three.png")
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBR' ,  "four.png")
        .settings.size(img_dim, img_dim)
    ,
    newText("trainInstr2", "Select the image that matches the sentence you just read.\n\n")
        .italic()
        .center()
        .print()
    ,    
    newCanvas( 'myCanvas', spr_canvas_dim, spr_canvas_dim)
        .center()
        .settings.add( img_left, img_top, getImage('imageTL'), 0 )
        .settings.add( img_right, img_top, getImage('imageTR'), 1 )
        .settings.add( img_right, img_bottom, getImage('imageBR'), 2 )
        .settings.add( img_left, img_bottom, getImage('imageBL'), 3 )
        .print()
     ,

    // Log canvas reaction time
    newVar("RT").global().set( v => Date.now() )
    ,
    newSelector( 'mySelector')
        .add( getImage('imageTL') , getImage('imageBL') , getImage('imageBR') , getImage('imageTR') )
        .wait()
        .log()
    ,
    getVar("RT").set( v => Date.now() - v )
        .log()
    ,
    newText("positive", "You passed this attention check!")  // Creation of a positive feedback text element
        .color("green")  // Green for positive (don't print yet)
        .css("font-size", "2em")
    ,
    newText("negative", "You failed the attention check. Please return the task in order to avoid having your submission rejected. Thank you!")   // Creation of a negative feedback text element
        .color("red")    // Red for negative (don't print yet)
        .css("font-size", "2em")
    ,
    getSelector("mySelector")           // Test whether the target image was selected
        .test.selected( getImage("imageBL") )
        .success(
            getText("positive")   // Positive feedback if the test succeeds
                .center()
                .print()
        )
        .failure(
            getText("negative")   // Negative feedback if the test fails
                .center()
                .print()
        )
   ,
    newButton("trainingButton", "Next")
        .center()
        .print()
        .wait()
    )
    // .log("List", "Attention")
    // .log("Guise", "Attention")
    // .log("ItemType", "Attention")
    // .log("ItemID", "Attention")
    // .log( "Cond", "Attention")
    // .log( "SentenceText", "Attention")
    // .log( "ImageTL", "Attention")
    // .log( "ImageTR", "Attention")
    // .log( "ImageBL", "Attention")
    // .log( "ImageBR", "Attention")
    // .log( "LabelTL", "Attention")
    // .log( "LabelTR", "Attention")
    // .log( "LabelBL", "Attention")
    // .log( "LabelBR", "Attention")
    // .log( "ReactionTime", getVar("RT"))
    // .log( "CompQ", "Attention") 
    // .log( "CompQ_Order", "Attention")
    // .log( "CompQ_Answer", "Attention") 
    // .log( "CompQ_Foil","Attention") 
//)


// Experiment instructions
PennController("exp-instr",
     newHtml("instructions_person1.html")
        .print()
     ,
     newButton("Start Experiment")
        .center()
        .print()
        .wait()
);


// Experiment Instructions from speech bubble

PennController("exp_instr_sb",
    newImage("sb_expinstr", "training_sb.png")
        .settings.size(sb_wide_width, sb_wide_height)
    ,
    newCanvas("expinstr_canvas", canv_wide_width, canv_wide_height)
        .center()
        .add(0, 0, getImage("sb_expinstr"))
        .print()
    ,
    newText("expinstr1", "You're getting the hang of this! Now we'll start the experiment.")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("expinstr_canvas"))
    ,
    newButton("expinstr_button1", "Next")
        .center()
        .print()
        .wait()
    ,
    getText("expinstr1").remove()
    ,
    getButton("expinstr_button1").remove()
    ,
    newText("expinstr2", "For the rest of the experiment, Taylor will be the one talking.<br>You'll get introduced in a moment.")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("expinstr_canvas"))
    ,
    newButton("expinstr_button2", "Next")
        .center()
        .print()
        .wait()
    ,
    getText("expinstr2").remove()
    ,
    getButton("expinstr_button2").remove()
    ,
    newText("expinstr3", "You might be from a different place from Taylor,<br>so it's OK if some things sound strange to you.")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("expinstr_canvas"))
    ,
    newButton("expinstr_button3", "Next")
        .center()
        .print()
        .wait()
    ,
    getText("expinstr3").remove()
    ,
    getButton("expinstr_button3").remove()
    ,    
    newText("expinstr4", "Just go with your first instinct to pick a picture and answer the question.<br><br>This won't affect your payment at the end.")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("expinstr_canvas"))
    ,
    newButton("expinstr_button4", "Next")
        .center()
        .print()
        .wait()
    ,
    getText("expinstr4").remove()
    ,
    getButton("expinstr_button4").remove()
    ,
    newText("expinstr5", "You'll be able to take breaks in the middle of the experiment.<br><br>Remember: try to go as quickly as you can while fully understanding the sentences!")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("expinstr_canvas"))
    ,
    newButton("expinstr_button5", "Start Experiment")
        .center()
        .print()
        .wait()
    ,
    getText("expinstr5").remove()
    ,
    getButton("expinstr_button5").remove()
    ,

    // Remove practice character and introduce experimental character
    getImage("sb_expinstr").remove()
    ,
    newImage("sb_guise1_wide", "personA_left.png")
        .settings.size(sb_wide_width, sb_wide_height)
        .print(0, 0, getCanvas("expinstr_canvas"))
    ,
    newText("expinstr6", "Hi, I'm Taylor. Let's get this started!")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("expinstr_canvas"))
    ,
    newButton("expinstr_button6", "Next")
        .center()
        .print()
        .wait()
);


// Experimental trials - block 1

Template( "dcc4_lists.csv" ,
    row => newTrial( "experiment_block1",
        newImage("sb1", "personA_left_inplace.png")
            .settings.size(sb_width, sb_height)
        ,
        newText("guise1_name", "Taylor")
        ,
        newController("ds1", "DashedSentence", {s: row.Sentence, mode: "speeded acceptability", blankText: "+", display: "in place"})
        ,
        newCanvas("sprcanv", canv_width, canv_height)
            .center()
            .add( sb_x_left, 0, getImage("sb1"))
            .add(ds_x_left, ds_y , getController("ds1"))
            .print()
        ,
        getController("ds1")
            .wait()
        ,
        getCanvas("sprcanv").remove()
    ,
   // Picture verification                 
    newImage('imageTL' ,  row.ImageTL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageTR' ,  row.ImageTR)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBL' ,  row.ImageBL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBR' ,  row.ImageBR)
        .settings.size(img_dim, img_dim)
    ,
    newCanvas( 'myCanvas', spr_canvas_dim, spr_canvas_dim)
        .center()
        .settings.add( img_left, img_top, getImage('imageTL'), 0 )
        .settings.add( img_right, img_top, getImage('imageTR'), 1 )
        .settings.add( img_right, img_bottom, getImage('imageBR'), 2 )
        .settings.add( img_left, img_bottom, getImage('imageBL'), 3 )
        .print()
     ,
    // Log canvas reaction time
    newVar("RT").global().set( v => Date.now() )
    ,
    newSelector( 'mySelector')
        .add( getImage('imageTL') , getImage('imageBL') , getImage('imageBR') , getImage('imageTR') )
        .wait()     
        .log()
    ,
    getVar("RT").set( v => Date.now() - v )
    ,
    getCanvas("myCanvas").remove()
    ,
    // Ask comprehension question
    newText(row.Question)
        .center()
        .bold()
        .css("font-size", "1.5em")
        .print()
    ,
    newText("option1", row.Option1)
        .css("font-size", "1.5em")
    ,
    newText("option2", row.Option2)
        .css("font-size", "1.5em")
    ,
    newScale("comp_question", getText("option1"), getText("option2"))
        .log()
        .center()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newButton("Next")
        .center()
        .print()
        .wait()
    )
    .log("List", row.Group)
    .log("Guise", row.Guise)
    .log("ItemType", row.ItemType)
    .log("ItemID", row.ItemID)
    .log( "Cond", row.Cond)
    .log( "Half", row.Half)
    .log( "SentenceText", row.Sentence)
    .log( "ImageTL", row.ImageTL)
    .log( "ImageTR", row.ImageTR)
    .log( "ImageBL", row.ImageBL)
    .log( "ImageBR", row.ImageBR)
    .log( "LabelTL", row.LabelTL)
    .log( "LabelTR", row.LabelTR)
    .log( "LabelBL", row.LabelBL)
    .log( "LabelBR", row.LabelBR)
    .log( "ReactionTime", getVar("RT"))
    .log( "CompQ", row.Question)
    .log( "CompQ_Order", row.OptionOrder)
    .log( "CompQ_Answer", row.Answer)
    .log( "CompQ_Foil", row.Foil)


)

// Experiment instructions
PennController("exp-instr2",
     newHtml("instructions_person2.html")
        .print()
     ,
     newButton("Start Part 2")
        .center()
        .print()
        .wait()
);

// Break
PennController("break",
    newImage("sb_expinstr", "personA_left.png")
        .settings.size(sb_wide_width, sb_wide_height)
    ,
    newCanvas("expinstr_canvas", canv_wide_width, canv_wide_height)
        .center()
        .add(0, 0, getImage("sb_expinstr"))
        .print()
    ,
    newText("expinstr1", "Take a break! Press next whenever you wanna start again.")
        .print(sb_wide_text_left, sb_wide_y, getCanvas("expinstr_canvas"))
    ,
    newButton("Next")
        .center()
        .print()
        .wait()
);

// Questions about the guises
newTrial("guiseq",
    newText("Thank you for completing the main part of the experiment!\nWe now have a few questions about your background.\nThe first few questions ask about the language from the person who was \"talking\" in the experiment.\n")
        .center()
        .italic()
        .print()
    ,
    // GUISE 1
    // Display guise 1 image as a reminder
    newImage("guise1_avatar", "personA.png")
        .settings.size(100,100)
    ,
    newCanvas("guise1_reminder", 150, 100)
        .settings.add(0, 50, newText("Taylor"))
        .settings.add(50, 0, getImage("guise1_avatar"))
        .print()
    ,

    newText("p1interloc-text", "On a scale from 1 to 5, how often do you interact with people who talk like Taylor?")
        .bold()
        .print()
    ,
    newScale("p1interloc",  "1 - Never", "2", "3 - Sometimes", "4", "5 - All the time")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newText("p1self-text", "On a scale from 1 to 5, to what degree do <i>you</i> talk like Taylor?")
        .bold()
        .print()
    ,
    newScale("p1self",  "1 - I never talk like this", "2", "3 - I sometimes talk like this", "4", "5 - I usually talk like this")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newButton("Next")
        .print()
        .wait()

)


newTrial("demo_html",
    newHtml("demographics.html")
        .log()
        .print()
    ,
    newButton("demo_next", "Next")
        .print()
        .wait()
)

newTrial("region",
    newText("Our final questions are about places you have lived.\nThis information is helpful for our research and will be kept confidential, but these questions are optional.\n")
        .italic()
        .print()
    ,  
    //Current Location 
    newText("Where do you currently live?")
        .bold()
        .print()
    ,
    newDropDown("currentState", "US State or Territory:")
        .log()
        .add('Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Florida', 'Georgia', 'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Minor Outlying Islands', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Puerto Rico', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'U.S. Virgin Islands', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming')
        .print()
    ,
    newText("Type of area")
        .bold()
        .print()
    ,
    newScale("currentArea", "Rural", "Suburban", "Urban", "Unsure/Prefer not to disclose")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newText("ZIP code:")
        .bold()
        .print()
    ,
    newTextInput("currentZIP")
        .settings.log()
        .settings.lines(1)
        .print()
    ,
    // Hometown
    newText("<b>Where did you grow up?<b>\n<i>If you lived in multiple places, pick one that you find most representative.</i>")
        .print()
    ,
    newDropDown("childState", "US State or Territory:")
        .log()
        .add('Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Florida', 'Georgia', 'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Minor Outlying Islands', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Puerto Rico', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'U.S. Virgin Islands', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming')
        .print()
    ,
    newText("Type of area")
        .bold()
        .print()
    ,
    newScale("childArea", "Rural", "Suburban", "Urban", "Unsure/Prefer not to disclose")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newText("ZIP code:")
        .bold()
        .print()
    ,
    newTextInput("childZIP")
        .settings.log()
        .settings.lines(1)
        .print()
    ,
    newText("Did you live in multiple regions during your childhood?")
        .bold()
        .print()
    ,
    newScale("childMultipleRegions", "Yes", "No") 
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newText(" ")
        .print()
    ,
    newButton("loca-next", "Next")
        .print()
        .wait()
)

newTrial("comment_page",
    newText("Optional: please leave any final comments you have about this study.")
        .bold()
        .print()
    ,
    newTextInput("comments")
        .settings.log()
        .settings.lines(0)
        .settings.size(400, 200)
        .print()
    ,
    newButton("finish_button", "Submit Study")
        .print()
        .wait()
)

// Goodbye and response code

// let alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz"; 

// newTrial( "bye" ,
//     newText("Thank you for your participation!\nPlease enter the code below for payment in MTurk:\n\n").print(),
//     newVar("ID", "")
//         .settings.global()
//         .set(v =>[...Array(8)].reduce(a=>a+alphanum.charAt(Math.floor(Math.random()*alphanum.length)),''))
//     ,
//     newText("code", "")
//         .settings.text( getVar("ID") ) 
//         .log()
//         .print()
//     ,
//     newButton().wait()  // Wait for a click on a non-displayed button = wait here forever
// )
newTrial( "bye",
         newHtml("prolific_redirect", "prolific_redirect.html")
            .print()
        ,
      newButton().wait() // Wait for a click on a non-displayed button = wait here forever 
)
.setOption( "countsForProgressBar" , false )
// Make sure the progress bar is full upon reaching this last (non-)trial